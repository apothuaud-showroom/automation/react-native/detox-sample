import React, { Component } from 'react';
import { Button } from 'react-native';

class NavBar extends Component {
    render() {
        return (
            <View >
                <Button>Home</Button>
                <Button>Login</Button>
            </View>
        );
    }
}

export default NavBar;